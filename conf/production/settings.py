from www.conf.settings import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

SITE_ID = 1

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Asia/Kolkata'
USE_I18N = True

ADMINS = (
    ('Vishwas Sharma', 'vishwas.sharma@hotmail.com'),
)

MANAGERS = ADMINS

# Make this unique, and don't share it with anybody.
SECRET_KEY = '+eh9m%7m0utj&amp;jy6dqileug+8+ym8y&amp;da0$2n1gyem2y709be3'

ROOT_URLCONF = 'www.conf.production.urls'

# -----------------------------------------------------
# ------------------ DATABASES ------------------------
# -----------------------------------------------------
static_app_path = "/home/vishwas/webapps/static_logicstick_com/"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(static_app_path, 'dev.db'),
    }
}

# -----------------------------------------------------
# -----------------------------------------------------
# -----------------------------------------------------
MEDIA_URL = '/uploads/'
STATIC_URL = 'http://static.logicstick.com/'
#STATIC_ROOT = os.path.join(static_app_path, 'static')
STATIC_ROOT = static_app_path
#STATIC_ROOT = os.path.join(PROJECT_DIR, PROJECT_MODULE_NAME, 'static')
#MEDIA_ROOT = os.path.join(VAR_ROOT, 'uploads')
MEDIA_ROOT = os.path.join(static_app_path, 'uploads')

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, PROJECT_MODULE_NAME, 'assets'),
)

# -----------------------------------------------------
# -----------------------------------------------------
# -----------------------------------------------------

PIPELINE_CSS = {
    'colors': {
        'source_filenames': (
          'css/style.css',
        ),
        'output_filename': 'css/style.min.css',
        'extra_context': {
            'media': 'screen,projection',
        },
    },
}

PIPELINE_JS = {
    'stats': {
        'source_filenames': (
          'js/plugins.js',
          'js/script.js',
          #'js/libs/*.js',
        ),
        'output_filename': 'js/stats.js',
    }
}


STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'
PIPELINE_YUI_BINARY = '/home/vishwas/bin/yui.sh' # path to yui binary
PIPELINE_STORAGE = 'pipeline.storage.PipelineFinderStorage'
# -----------------------------------------------------
# -----------------------------------------------------
MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'pipeline.middleware.MinifyHTMLMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    #'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)



INSTALLED_APPS += (
    #'django.contrib.admin',
    #'django.contrib.admindocs',

    # Install dependencies and external project
    'pipeline',
    'manifesto',
)

