from django.conf.urls.defaults import patterns, url, include

urlpatterns = patterns('',
    # project urls here
    url(r'^$', 'www.apps.main.views.home', name='home'),
)
