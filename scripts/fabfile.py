from __future__ import with_statement
from fabric.api import local, settings, abort, run, cd, env, sudo
from fabric.colors import green as _green
from fabric.colors import yellow as _yellow
from fabric.colors import red as _red
from fabric.contrib.console import confirm
from fabric.contrib.project import rsync_project
from fabric.contrib.files import upload_template, exists
from fabric.operations import require
from fabric.context_managers import prefix
import os.path
import inspect
import sys

    
def _set_webfaction_env():
    env.python = 'python2.7'
    #env.repo_url = "git@bitbucket.org:vishwassharma/logicstick.com.git"
    env.repo_url = "https://vishwassharma@bitbucket.org/vishwassharma/logicstick.com.git"
    
    env.home = os.path.join('/home', env.webfaction_username)
    env.path = os.path.join(env.home, 'webapps', env.webfaction_appname)
    env.repo_path = os.path.join(env.path, 'www')
    env.env_path  = os.path.join(env.home, 'work', env.virtualenv)

    env.cmd_apache_start = '%(path)s/apache2/bin/start' % env
    env.cmd_apache_stop = '%(path)s/apache2/bin/stop' % env
    env.cmd_apache_restart = '%(path)s/apache2/bin/restart' % env

# -----------------------------------------------------------------------------
# Environments
# -----------------------------------------------------------------------------

def development():
    """
    Select development environment for commands
    """
    env.settings = 'development'
    env.webfaction_username = 'vishwas'
    env.webfaction_appname = 'dev_logicstick_com'
    env.hosts = ['vishwas@dev.logicstick.com']
    env.virtualenv = "dev_logicstick_com"
    env.requirements = 'development.txt'
    _set_webfaction_env()

    
def production():
    """
    Select production environment for commands
    """
    if not confirm(_red('Production environment selected, are you sure?'), default=False):
        return
    env.settings = 'production'
    env.webfaction_username = 'vishwas'
    env.webfaction_appname = 'logicstick_com'
    env.hosts = ['vishwas@logicstick.com']
    env.virtualenv = "logicstick_com"
    env.requirements = 'production.txt'
    _set_webfaction_env()

# -----------------------------------------------------------------------------
# Branches
# -----------------------------------------------------------------------------

def master():
    """
    Work on master branch.
    """
    env.branch = 'master'

def branch(branch_name):
    """
    Work on any specified branch.
    """
    env.branch = branch_name

# -----------------------------------------------------------------------------
# Main commands
# -----------------------------------------------------------------------------

def setup():
    """
    Setup a fresh virtualenv, install everything we need, and fire up the database.
    """
    require('settings', provided_by=[production, development,])
    rmdirs()
    #setup_virtualenv()
    clone_repo()
    checkout_latest()
    install_requirements()
    #drop_database();
    #create_database()
    #populate_database()
    install_apache_conf()
    install_django_bin()
    install_django_conf()
    apache_restart()

def deploy():
    """
    Deploy the latest version of the site to the server and restart apache.
    
    Only works for webfaction servers.
    """
    require('settings', provided_by=[production, development])
    require('webfaction_appname', provided_by=[production, development])
    
    maintenance_on()
    apache_stop()
    checkout_latest()
    install_requirements()
    install_django_bin()
    install_django_conf()
    #migrate()
    maintenance_off()
    apache_start()

def rollback(commit_id):
    """
    Rolls back to specified git commit hash or tag. There is NO guarantee we
    have committed a valid dataset for an arbitrary commit hash.

    Only works for webfaction servers.
    """
    require('settings', provided_by=[production, development,])
    maintenance_on()
    apache_stop()
    checkout_latest()
    git_reset(commit_id)
    install_requirements()
    install_django_bin()
    install_django_conf()
    #migrate()
    maintenance_off()
    apache_start()

def maintenance_on():
    """
    Turn maintenance mode on.
    """
    print(_yellow('>>> starting %s()' % _fn()))
    require('settings', provided_by=[development, production])
    run('touch %(repo_path)s/.upgrading' % env)

def maintenance_off():
    """
    Turn maintenance mode off.
    """
    print(_yellow('>>> starting %s()' % _fn()))
    require('settings', provided_by=[development, production])
    run('rm -f %(repo_path)s/.upgrading' % env)

def rmdirs():
    """Remove the default directory"""
    require('settings', provided_by=[production, development,])
    with cd ("%(path)s" % env):
        run("rm -rf myproject")

def clone_repo():
    """
    Do initial clone of the git repo.
    """
    print(_yellow('>>> starting %s()' % _fn()))
    run('git clone %(repo_url)s %(repo_path)s' % env)

def checkout_latest():
    """
    Pull the latest code on the specified branch.
    """
    print(_yellow('>>> starting %s()' % _fn()))

    require('branch', provided_by=[master, branch])
    with cd(env.repo_path):
        run('git pull origin %(branch)s' % env)
        run('git checkout %(branch)s || git checkout -b %(branch)s origin/%(branch)s' % env)
        run('git pull' % env)

def install_requirements():
    """
    Install the required packages using pip.
    """
    print(_yellow('>>> starting %s()' % _fn()))
    virtualenv('pip install -q -r %(repo_path)s/requirements/%(requirements)s' % env)
    

def virtualenv(command):
    """
    Run command in virtualenv.
    """
    with prefix('source %(env_path)s/bin/activate' % env):
        run(command)

def apache_restart():
    """
    Restarts apache
    """
    print(_yellow('>>> starting %s()' % _fn()))
    require('settings', provided_by=[development, production])
    run(env.cmd_apache_restart)

def apache_start():
    """
    Starts apache
    """
    print(_yellow('>>> starting %s()' % _fn()))
    require('settings', provided_by=[development, production])
    run(env.cmd_apache_start)
    

def apache_stop():
    """
    Stops apache
    """
    print(_yellow('>>> starting %s()' % _fn()))
    require('settings', provided_by=[development, production])
    run(env.cmd_apache_stop)

def install_apache_conf():
    """
    Backup old httpd.conf and install new one.
    """
    print(_yellow('>>> starting %s()' % _fn()))
    require('settings', provided_by=[development, production])

    # get port apache is listening on
    env.port = run('grep "Listen" %(path)s/apache2/conf/httpd.conf | cut -f2 -d " "' % env)
    print(_yellow('*** apache is listening on port %(port)s' % env))

    upload_template('templates/apache/webfraction.conf.template', '%(path)s/apache2/conf/httpd.conf' % env,
                    context=env, use_jinja=True)

    #upload_template('apache/config.wsgi.template', '%(path)s/apache2/conf/%(project_name)s.wsgi' % env,
                    #context=env, use_jinja=True)

#install_django_bin()
def install_django_bin():
    """Installaing bin/manager.py"""
    print(_yellow(">> starting %s()" % _fn()))
    require('settings', provided_by=[development, production])
    upload_template("templates/apache/manage.py.template", "%(repo_path)s/bin/manage.py" % env, 
                    context=env, use_jinja=True)

#install_django_conf()
def install_django_conf():
    """Install the django configuration"""
    print(_yellow(">> starting %s()" % _fn()))
    require('settings', provided_by=[development, production])
    upload_template("templates/apache/wsgi.py.template", "%(repo_path)s/conf/wsgi.py" % env, 
                    context=env, use_jinja=True)

    if env.settings == "production":
        with cd("%(repo_path)s" % env):
            virtualenv("python bin/manage.py collectstatic")
# -----------------------------------------------------------------------------
# Functions not to be run directly
# -----------------------------------------------------------------------------
def _fn():
    """
    Returns current function name
    """
    return inspect.stack()[1][3]

