from manifesto import Manifest


class StaticManifest(Manifest):
  def cache(self):
    return [
            'style.min.css',
            'stats.js'
    ]

  def network(self):
    return ['*']

  def fallback(self):
    return [
      ('/', '/404.html'),
    ]
